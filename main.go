package main 

import "fmt"
import "log"
import "strings"
import "strconv"
import _ "github.com/lib/pq"
import "database/sql" 
import "time"
import "github.com/streadway/amqp"

const DB_CONNECT_STRING = "host=localhost port=5432 user=postgres password=1 dbname=boilers_hef sslmode=disable"

func main() {
	fmt.Println("HEF Boiler data saver")
	fmt.Println("Reading data from queue...")
	conn, err := amqp.Dial("amqp://ms:msqA115@localhost:5672")
	failOnError( err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()


	err = ch.ExchangeDeclare(
		"vals",
		"fanout",
		false,
		false,
		false,
		false,
		nil,
		)

	failOnError(err, "Failed to declare an exchange")

	q, err := ch.QueueDeclare(
		"",
		false,
		false,
		true,
		false,
		nil,
		)

	failOnError(err, "Failed to declare a queue")

	err = ch.QueueBind(
		q.Name,
		"",
		"vals",
		false,
		nil)
	failOnError(err, "Failed to bind a queue")

	msgs, err := ch.Consume(
		q.Name,
		"",
		true,
		false,
		false,
		false,
		nil,
		)
	failOnError(err, "Failed to register a consumer")

	fmt.Println("Saving data to db...")
	db, err := sql.Open("postgres", DB_CONNECT_STRING)
	defer db.Close()

	if err != nil {
		fmt.Printf("Database opening error --> %v\n", err)
		panic("Database error")
	}

	forever := make(chan bool)
		go func(){
			for d := range msgs {
				s := strings.Split( string(d.Body), " ")
				fmt.Printf(" [x] %s %s %s\n", s[0], s[1], s[2])
				id, err := strconv.Atoi(s[0])
				failOnError(err, "Unable to convert id to int")
				val, err := strconv.ParseFloat(s[2], 32)
				failOnError(err, "Unable to parse value to float")

				make_insertion(&db, int32(id), float32(val) )

			}
		}()
		log.Printf(" [*] Waiting for values. To exit press CTRL+C")
		<-forever



}


func make_insertion(pdb **sql.DB, id int32, value float32){
	db := *pdb
	const INSERT_QUERY = `insert into public.params(chanel_id, "value", date_query, "valid") values ($1, $2, $3, true)`

	insert_query, err := db.Prepare(INSERT_QUERY)
	defer insert_query.Close()

	if err != nil {
		fmt.Printf("Query preparation error --> %v\n", err)
		panic("Insert query error")
	}

	t1 := time.Now()

	_, err = insert_query.Exec(id, value, t1)

	if err != nil {
		fmt.Printf("Query execution error --> %v\n", err)
		panic("Error")
	}
}

func failOnError(err error, msg string){
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
		panic( fmt.Sprintf("%s: %s", msg, err))
	}
}


